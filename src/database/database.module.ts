import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from '@nestjs/config';

@Module({
    imports: [
        TypeOrmModule.forRootAsync({
            useFactory: (configService: ConfigService) => ({
                type: 'postgres',
                host: configService.get('PG_HOST'),
                port: parseInt(configService.getOrThrow('PG_PORT'), 10),
                username: configService.get('PG_USERNAME'),
                password: configService.get('PG_PASSWORD'),
                database: configService.getOrThrow('PG_DATABASE'),
                autoLoadEntities: true,  
                synchronize:false
            }),
            inject: [ConfigService] 
        })
    ]
})
export class DatabaseModule { }
