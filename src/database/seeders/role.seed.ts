import { Role } from 'src/users/entities/role.entity';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class RolesSeeder implements Seeder {
  public async run(
    dataSource: DataSource
  ): Promise<any> {
    const repository = dataSource.getRepository(Role);

    const existingRoles = await repository.count();

    if (existingRoles === 0) {
      const rolesToSeed = [
        { name: 'Admin' },
        { name: 'Executive'},
        { name: 'Staff'},
      ];

      await repository.save(rolesToSeed);
    }
  }
}