import { DataSource } from 'typeorm';
import { runSeeders, Seeder } from 'typeorm-extension';
import RolesSeeder from './role.seed';

export default class InitSeeder implements Seeder {
  public async run(
    dataSource: DataSource,
  ): Promise<any> {
    await runSeeders(dataSource, {
      seeds: [RolesSeeder],
    });
  }
}