import { Role } from "../entities/role.entity";

export class CreateUserDto {
    role: Role;
    email: string;
    password: string;
}
