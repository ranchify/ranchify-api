import { z } from 'zod';

export const CreateUserSchema = z.object({
    email: z.string().email().nonempty(),
    password: z.string().nonempty(),
    role: z.string().nonempty()
});