import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Role } from "./role.entity";

enum Status {
    ACTIVE = 'active',
    INACTIVE = 'inactive'
};

@Entity()
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    email: string;

    @Column()
    password: string;
     
    @Column({
        type: 'enum',
        enum: Status,
        default: Status.ACTIVE
    })
    status: string; 

    @Column({name: 'created_at', default: () => 'CURRENT_TIMESTAMP'}) 
    createdAt: Date;

    @ManyToOne(() => Role, role => role.users)
    @JoinColumn({name: 'role_id'})
    role: Role;

    constructor(user: Partial<User>)
    {
        Object.assign(this, user);
    }

}
