import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./user.entity";

enum Status {
    ACTIVE = 'active',
    INACTIVE = 'inactive'
};

@Entity()
export class Role {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        type: 'enum',
        enum: Status,
        default: Status.ACTIVE
    })
    status: string; 

    @Column()
    name: string;

    @Column({name: 'created_at'}) 
    createdAt: Date;

    @OneToMany(() => User, user => user.role)
    users: User[];

    constructor(role: Partial<Role>){
        Object.assign(this, role);
    }

}