import { MigrationInterface, QueryRunner } from "typeorm";

export class Role1716669294635 implements MigrationInterface {


    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
          CREATE TABLE "role" (
            "id" uuid NOT NULL,
            "status" varchar NOT NULL DEFAULT 'active',
            "name" varchar(255) NOT NULL,
            "created_at" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY ("id")
          );
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "role"`);
    }

}
