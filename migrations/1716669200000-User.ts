import { MigrationInterface, QueryRunner } from "typeorm";

export class User1716669123950 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        // await queryRunner.query(`
        //     CREATE TYPE "status_enum" AS ENUM('active', 'inactive')
        // `);

        // await queryRunner.query(`
        //     CREATE TABLE "user" (
        //         "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
        //         "email" varchar(255) NOT NULL,
        //         "password" varchar(255) NOT NULL,
        //         "status" "status_enum" NOT NULL DEFAULT 'active',
        //         "created_at" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        //         "role_id" uuid,
        //         PRIMARY KEY ("id"),
        //         CONSTRAINT "FK_role" FOREIGN KEY ("role_id") REFERENCES "role"("id")
        //     )
        // `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "user"`);
    }

}
