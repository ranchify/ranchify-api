FROM node:18.6.0-alpine3.16

WORKDIR /ranchify-api

COPY package.json ./

RUN npm install

COPY . .

EXPOSE 3000

RUN npm run build

CMD npm run migration:run && npm run start:dev

# docker compose -f docker-compose.yml up -d --build
# db string connection jdbc:postgresql://localhost:5433/horizonhub
# docker compose -f docker-compose.yml up -d --build


# stop dockers sudo docker-compose down