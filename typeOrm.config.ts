import { ConfigService } from '@nestjs/config';
import { config } from 'dotenv';
import { Role } from './src/users/entities/role.entity';
import { User } from './src/users/entities/user.entity';
import InitSeeder from 'src/database/seeders/init.seed';
import { ConfigModule } from '@nestjs/config';
import { DataSource } from 'typeorm';
import { SeederOptions } from 'typeorm-extension';
import { DataSourceOptions } from 'typeorm/data-source';

config();

const configService = new ConfigService();

const options = {
    type: 'postgres',
    host: configService.getOrThrow('PG_HOST'),
    port: configService.getOrThrow('PG_PORT'),
    username: configService.getOrThrow('PG_USERNAME'),
    password: configService.getOrThrow('PG_PASSWORD'),
    database: configService.getOrThrow('PG_DATABASE'),
    migrations: ['migrations/**'],
    seeds: [],
    entities: [
        User,
        Role
    ]
};

export const source = new DataSource(
    options as DataSourceOptions & SeederOptions,
);